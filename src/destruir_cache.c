#include <stdlib.h>
#include<stdio.h>
#include "slaballoc.h"
#include "objetos.h"

//Destruimos el cache. Solo lo destruimos si no hay ninguna asignacion
//(cantidad_en_uso == 0). 
//No olvide no solo liberar la memoria del cache
//sino tambien la memoria de la informacion de slabs y del puntero cache
//tambien.
void destruir_cache(SlabAlloc *cache){
	//Solo se destruye el cache si no está siendo usado
	if(cache->cantidad_en_uso == 0){
		/* Llamo al destructor asignado para el objeto que creamos*/
		for(int i=0;i<cache->tamano_cache;i++){
			cache->destructor((cache->slab_ptr->ptr_data),cache->tamano_objeto);
		}
		/*Primero hago null parte de la estructura del SlabAlloc*/						
		cache->slab_ptr->ptr_data=NULL;;		
		cache->slab_ptr=NULL;	
		cache->mem_ptr=NULL;
		cache->nombre = NULL;
		cache=NULL;
		
		
		/* Libero la memoria*/		
		free(cache);
		
	}
	else{
		printf("ADVERTENCIA:\n");
		printf("Tiene elementos en uso y no es posible borrar el caché en esas condiciones \n");
	}

}
