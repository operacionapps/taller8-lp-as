#include <stdlib.h>
#include<stdio.h>
#include "slaballoc.h"
#include "objetos.h"

//Muestra el estado del cache. El formato de salida es:
//
//Nombre de cache: 		<nombre>
//Cantidad de slabs: 		<numero>
//Cantidad de slabs en uso	<numero>
//Tamano de objeto		<tamano en bytes>
//
//Direccion de cache->mem_ptr	<direccion del bloque de memoria de cache de objetos>
//direccion ptr[0].ptr_data: 	<direccion en hex>  <EN_USO|DISPONIBLE>, ptr[0].ptr_data_old: 	<direccion en hex>
//direccion ptr[1].ptr_data: 	<direccion en hex>  <EN_USO|DISPONIBLE>, ptr[1].ptr_data_old: 	<direccion en hex>
//direccion ptr[2].ptr_data: 	<direccion en hex>  <EN_USO|DISPONIBLE>, ptr[2].ptr_data_old: 	<direccion en hex>
//...

void stats_cache(SlabAlloc *cache){
	if(cache->mem_ptr == NULL){
		printf("Nombre del cache: %s\n",cache->nombre );
		printf("Cantidad de slabs: %d\n",cache->tamano_cache );
		printf("Cantidad de slabs en uso: %d\n",cache->cantidad_en_uso );
		printf("Tamano de objeto: %ld bytes\n\n",cache->tamano_objeto); //en bytes
		printf("ATENCIÓN:");
		printf("No hay elementos en el caché\n");
		printf("No hay datos que mostrar \n");
	}
	else{
	printf("Nombre del cache: %s\n",cache->nombre );
		printf("Cantidad de slabs: %d\n",cache->tamano_cache );
		printf("Cantidad de slabs en uso: %d\n",cache->cantidad_en_uso );
		printf("Tamano de objeto: %ld bytes\n\n",cache->tamano_objeto*4 ); //en bytes 
		printf("Direccion de cache->mem_ptr: <%p>\n",cache->mem_ptr );
		int i=0;
		for(i=0;i<cache->tamano_cache;i++){
			//Ya que usamos numeros para evaluar si está disponible, entonces hacemos un if para que se presente el formato deseado
			if((cache->slab_ptr+i)->status==1){
				printf("direccion ptr[%d].ptr_data: <%p> <DISPONIBLE>, ptr[%d].ptr_data_old: <%p>\n",i,(cache->slab_ptr+i)->ptr_data,i,(cache->slab_ptr+i)->ptr_data_old );
			}
			else{
				printf("direccion ptr[%d].ptr_data: <%p> <EN_USO>, ptr[%d].ptr_data_old: <%p>\n",i,(cache->slab_ptr+i)->ptr_data,i,(cache->slab_ptr+i)->ptr_data_old );
			}
			
	}
	}
	
}
